<?php
/**
 * Created by PhpStorm.
 * User: НЮКазанков
 * Date: 21.09.2018
 * Time: 16:26
 */

namespace frontend\controllers\auth;

use Yii;
use yii\base\Module;
use yii\web\Controller;
use shop\forms\auth\LoginForm;
use shop\services\auth\AuthService;

class AuthController extends Controller
{
    private $authService;

    public function __construct($id, Module $module, AuthService $authService, array $config = [])
    {
        $this->authService = $authService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $user = $this->authService->auth($form);
                Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
                return $this->goBack();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}